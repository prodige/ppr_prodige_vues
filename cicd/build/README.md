# Build

## Execute build process locally

The build step is a pipeline described in `Jenkinsfile`.

The step are run in order with bash scripts as [number]_[script_name].sh

Exemple, run one by one :

```bash
./0_fetch-dependencies.sh
./5_make_release.sh
./6_make_docker.sh
```

Now you can test the docker you built with [../test-prod/README.md](../test-prod/README.md)

If an error occurs, use `99_clean.sh` to clean everything and revert rights.

## Advanced

### Use nexus

To use nexus.alkante.al to get composer and/or npm package, you need to uncomment lines with NEXUS, NEXUS_USR and NEXUS_PSW in files:
- ./0_fetch-dependencies.sh
- ./Jenkinsfile

### Run assetic

To run assetic, you need to uncomment "Make asset cache" step in :
- ./Jenkinsfile
