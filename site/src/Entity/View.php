<?php

namespace App\Entity;

use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Attributes as OA;

class View
{
    #[Groups(['post', 'patch'])]
    #[Assert\Type('string')]
    #[Assert\NotBlank(groups: ['post', 'patch'])]
    #[SerializedName('view_name')]
    #[OA\Property(property: 'view_name', example: 'departements_view')]
    public string $viewName;

    #[Groups(['post', 'patch'])]
    #[Assert\Type('string')]
    #[Assert\NotBlank(groups: ['post', 'patch'])]
    #[OA\Property(property: 'resource', example: '3c892ef0-0a6d-11de-ad6b-00104b7907b4')]
    public string $resource;

    #[Groups(['post'])]
    #[Assert\Type('bool')]
    #[OA\Property(property: 'is_customized')]
    public bool $isCustomized = false;

    #[Groups(['post', 'patch'])]
    #[Assert\Type('string')]
    #[Assert\NotBlank(groups: ['post', 'patch'])]
    #[OA\Property(property: 'dataset', example: 'departements')]
    public string $dataset;

    #[Groups('patch')]
    #[Assert\All(new Assert\Type(Join::class))]
    #[OA\Property(type: 'array', items: new OA\Items(ref: new Model(type: Join::class)))]
    public array $joins;

    #[Groups('patch')]
    #[OA\Property(property: 'calculated_fields', type: 'array',
        items: new OA\Items(
            properties: [
                new OA\Property(property: 'name', type: 'string', example: 'fieldName'),
                new OA\Property(property: 'expression', type: 'string', example: 't1_population/c_superficie'),
            ],
            type: 'object'
        )
    )]
    public array $calculatedFields = [];

    #[Groups('patch')]
    #[Assert\All(new Assert\Type(type: 'string'))]
    #[OA\Property(property: 'visible_fields', type: 'array', items: new OA\Items(type: 'string'), example: [
        'field1',
        'field2',
        'field3',
    ])]
    public array $visibleFields = [];

    #[Groups('patch')]
    #[Assert\All(new Assert\Type(type: 'string'))]
    #[OA\Property(property: 'grouping_fields', type: 'array', items: new OA\Items(type: 'string'), example: [
        'field1',
        'field2',
        'field3',
    ])]
    public array $groupingFields;

    #[Groups('patch')]
    #[Assert\All(new Assert\Type(type: 'string'))]
    #[OA\Property(property: 'grouping_method', type: 'array', items: new OA\Items(type: 'string'), example: [
        'the_geom' => 'st_collect',
        'field3' => 'sum',
    ], additionalProperties: new OA\AdditionalProperties(type: 'string'))]
    public array $groupingMethod;

    #[Groups('patch')]
    #[Assert\Type('string')]
    #[OA\Property(example: 'expression')]
    public string $filter = '';

    #[Groups('patch')]
    #[OA\Property(property: 'sort',
        type: 'array', items: new OA\Items(
            properties: [
                new OA\Property(property: 'field', type: 'string', example: 'field2'),
                new OA\Property(property: 'method', type: 'string', example: 'asc'),
            ],
            type: 'object'
        ), maxLength: 1
    )]
    public array $sort;

    #[Groups('patch')]
    #[OA\Property(property: 'view_sql')]
    public string $viewSql;

    /**
     * @return string
     */
    public function getViewName(): string
    {
        return $this->viewName;
    }

    /**
     * @param string $viewName
     * @return View
     */
    public function setViewName(string $viewName): View
    {
        $this->viewName = $viewName;

        return $this;
    }

    /**
     * @return string
     */
    public function getResource(): string
    {
        return $this->resource;
    }

    /**
     * @param string $resource
     * @return View
     */
    public function setResource(string $resource): View
    {
        $this->resource = $resource;

        return $this;
    }

    /**
     * @return string
     */
    public function getDataset(): string
    {
        return $this->dataset;
    }

    /**
     * @param string $dataset
     * @return View
     */
    public function setDataset(string $dataset): View
    {
        $this->dataset = $dataset;

        return $this;
    }

    /**
     * @return array
     */
    public function getJoins(): array
    {
        return $this->joins;
    }

    /**
     * @param array $joins
     * @return View
     */
    public function setJoins(array $joins): View
    {
        $this->joins = $joins;

        return $this;
    }

    /**
     * @return array
     */
    public function getCalculatedFields(): array
    {
        return $this->calculatedFields;
    }

    /**
     * @param array $calculatedFields
     * @return View
     */
    public function setCalculatedFields(array $calculatedFields): View
    {
        $this->calculatedFields = $calculatedFields;

        return $this;
    }

    /**
     * @return array
     */
    public function getVisibleFields(): array
    {
        return $this->visibleFields;
    }

    /**
     * @param array $visibleFields
     * @return View
     */
    public function setVisibleFields(array $visibleFields): View
    {
        $this->visibleFields = $visibleFields;

        return $this;
    }

    /**
     * @return array
     */
    public function getGroupingFields(): array
    {
        return $this->groupingFields;
    }

    /**
     * @param array $groupingFields
     * @return View
     */
    public function setGroupingFields(array $groupingFields): View
    {
        $this->groupingFields = $groupingFields;

        return $this;
    }

    /**
     * @return array
     */
    public function getGroupingMethod(): array
    {
        return $this->groupingMethod;
    }

    /**
     * @param array $groupingMethod
     * @return View
     */
    public function setGroupingMethod(array $groupingMethod): View
    {
        $this->groupingMethod = $groupingMethod;

        return $this;
    }

    /**
     * @return string
     */
    public function getFilter(): string
    {
        return $this->filter;
    }

    /**
     * @param string $filter
     * @return View
     */
    public function setFilter(string $filter): View
    {
        $this->filter = $filter;

        return $this;
    }

    /**
     * @return array
     */
    public function getSort(): array
    {
        return $this->sort;
    }

    /**
     * @param array $sort
     * @return View
     */
    public function setSort(array $sort): View
    {
        $this->sort = $sort;

        return $this;
    }

    public function isCustomized(): bool
    {
        return $this->isCustomized;
    }

    public function setIsCustomized(bool $isCustomized): View
    {
        $this->isCustomized = $isCustomized;

        return $this;
    }

    public function getViewSql(): string
    {
        return $this->viewSql;
    }

    public function setViewSql(string $viewSql): View
    {
        $this->viewSql = $viewSql;

        return $this;
    }
}
