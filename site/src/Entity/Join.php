<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use phpDocumentor\Reflection\Types\Collection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Attributes as OA;

class Join
{
    #[Groups('patch')]
    #[Assert\Type('string')]
    #[Assert\NotBlank(groups: ['patch'])]
    #[OA\Property(property: 'resource', example: '3c892ef0-0a6d-11de-ad6b-00104b7907b4')]
    public string $resource;

    #[Groups('patch')]
    #[Assert\Type('string')]
    #[Assert\NotBlank(groups: ['patch'])]
    #[OA\Property(property: 'dataset', example: 'departements')]
    public string $dataset;

    #[Groups('patch')]
    #[Assert\Type('string')]
    #[Assert\NotBlank(groups: ['patch'])]
    #[OA\Property(property: 'field_view', example: 'code_insee')]
    public string $fieldView;

    #[Groups('patch')]
    #[Assert\Type('string')]
    #[Assert\NotBlank(groups: ['patch'])]
    #[OA\Property(property: 'field_table', example: 'insee')]
    public string $fieldTable;

    #[Groups('patch')]
    #[Assert\Type('bool')]
    #[OA\Property(property: 'restrictive', example: true)]
    public bool $restrictive;

    #[Groups('patch')]
    #[Assert\NotBlank(groups: ['patch'])]
    #[Assert\All(new Assert\Type(type: 'string'))]
    #[OA\Property(property: 'visible_fields', type: 'array', items: new OA\Items(type: 'string'), example: [
        'field1',
        'field2',
        'field3',
    ])]
    public array $visibleFields;

    /**
     * @return string
     */
    public function getResource(): string
    {
        return $this->resource;
    }

    /**
     * @param string $resource
     * @return Join
     */
    public function setResource(string $resource): Join
    {
        $this->resource = $resource;

        return $this;
    }

    /**
     * @return string
     */
    public function getDataset(): string
    {
        return $this->dataset;
    }

    /**
     * @param string $dataset
     * @return Join
     */
    public function setDataset(string $dataset): Join
    {
        $this->dataset = $dataset;

        return $this;
    }

    /**
     * @return string
     */
    public function getFieldView(): string
    {
        return $this->fieldView;
    }

    /**
     * @param string $fieldView
     * @return Join
     */
    public function setFieldView(string $fieldView): Join
    {
        $this->fieldView = $fieldView;

        return $this;
    }

    /**
     * @return string
     */
    public function getFieldTable(): string
    {
        return $this->fieldTable;
    }

    /**
     * @param string $fieldTable
     * @return Join
     */
    public function setFieldTable(string $fieldTable): Join
    {
        $this->fieldTable = $fieldTable;

        return $this;
    }

    /**
     * @return bool
     */
    public function isRestrictive(): bool
    {
        return $this->restrictive;
    }

    /**
     * @param bool $restrictive
     * @return Join
     */
    public function setRestrictive(bool $restrictive): Join
    {
        $this->restrictive = $restrictive;

        return $this;
    }

    /**
     * @return array
     */
    public function getVisibleFields(): array
    {
        return $this->visibleFields;
    }

    /**
     * @param array $visibleFields
     * @return Join
     */
    public function setVisibleFields(array $visibleFields): Join
    {
        $this->visibleFields = $visibleFields;

        return $this;
    }
}
