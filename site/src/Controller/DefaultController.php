<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    #[Route('/logout', name: 'app_logout', methods: ['GET'])]
    public function logout(): Response
    {
        return $this->redirect('/prodige/disconnectAll');
    }

    #[Route('/', name: 'homepage', methods: ['GET'])]
    public function home(): Response
    {
        return $this->redirect('/api/doc');
    }
}
