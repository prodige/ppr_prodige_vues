<?php

namespace App\Controller;

use App\Entity\Join;
use App\Entity\View;
use App\Service\CatalogueService;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use Doctrine\Persistence\ManagerRegistry;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Attributes as OA;
use Prodige\ProdigeBundle\Common\DBManager\ViewFactory;
use Prodige\ProdigeBundle\Services\AdminClientService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/api/view')]
class ApiController extends AbstractController
{

    private Connection $prodige;
    private ViewFactory $factory;

    /**
     * @throws Exception
     */
    public function __construct(
        ManagerRegistry $doctrine,
        private readonly CatalogueService $catalogueService,
        private readonly ValidatorInterface $validator,
        private readonly SerializerInterface $serializer,
        private AdminClientService $adminClientService,
        private ParameterBagInterface $parameterBag


    ) {
        $this->prodige = $doctrine->getConnection('prodige');
        $this->prodige->isTransactionActive() && $this->prodige->beginTransaction();
        $this->factory = ViewFactory::getInstance($this->prodige);
    }

    /**
     * @throws Exception
     */
    #[Route('/{uuid}', name: 'get_view', methods: ['GET'])]
    #[OA\Tag('View')]
    public function getViewAction(string $uuid): Response
    {
        $options = 'TRAITEMENTS=CMS&OBJET_TYPE=dataset&uuid='.$uuid;
        $right = $this->adminClientService->curlAdminWithAdmincli(
            $this->parameterBag->get('ADMIN_URL_API').'/internal/verify_right?'.$options,
            'GET'
        );

        if (is_array($right) && array_key_exists('error', $right)) {
            return new JsonResponse([
                'error' => "ERREUR : Vos droits n'ont pas pu être récupérés",
                'success' => false,
            ]);
        }

        if (isset($right['right']['CMS']) && $right['right']['CMS'] === true) {
            $viewId = $this->catalogueService->getViewFromUuid($uuid);
            if (!$viewId) {
                return new JsonResponse(['error' => "Unknow view for uuid : $uuid"], 404);
            }
            $viewInfo = $this->catalogueService->getViewInfo($viewId);
            if (!$viewInfo) {
                return new JsonResponse(['error' => "Unknow view for uuid : $uuid"], 404);
            }

            $response = [
                'view_name' => $viewInfo['view_name'],
                'resource' => $uuid,
                'dataset' => $viewInfo['layer_name'],
            ];

            if ($viewInfo['view_type'] == ViewFactory::$VIEW_TYPE_CUSTOMIZED) {
                $response['view_sql'] = $viewInfo['view_sql'];
                $response['is_customized'] = true;
            } else {
                $response['joins'] = $this->catalogueService->getViewJoins($viewId);
                $response['calculated_fields'] = $this->catalogueService->getViewCalculatedFields($viewId);
                $response['visible_fields'] = (!empty($viewInfo['layer_fields'])) ? explode(
                    ',',
                    $viewInfo['layer_fields']
                ) : [];
                $response = array_merge($response, $this->catalogueService->getAggregateFields($viewId));
                $response['filter'] = (!empty($viewInfo['filter_expression'])) ? $viewInfo['filter_expression'] : '';
                $sort = [];
                if (array_key_exists('sorting_field', $viewInfo) && array_key_exists('sorting_method', $viewInfo)) {
                    if ($viewInfo['sorting_field'] !== 'null' && $viewInfo['sorting_method'] !== 'null') {
                        $sort = ['field' => $viewInfo['sorting_field'], 'method' => $viewInfo['sorting_method']];
                    }
                }
                $response['sort'] = (count($sort) > 0) ? [$sort] : [];
                $response['is_customized'] = false;
            }
        } else {
            if (!array_key_exists('fmeta_id', $right['right'])) {
                $response = [
                    'error' => "ERREUR : la donnée ".$uuid." n'existe pas",
                    'success' => false,
                ];
            } else {
                $response = [
                    'error' => "ERREUR : Vos droits ne vous permettent pas d'accéder à la donnée ".$uuid,
                    'status' => 403,
                ];
            }
        }

        return new JsonResponse($response);
    }

    /**
     * @throws Exception
     */
    #[Route(name: 'post_view', methods: ['POST'])]
    #[OA\RequestBody(content: new Model(type: View::class, groups: ['post']))]
    #[OA\Tag('View')]
    public function postViewAction(Request $request): Response
    {
        $view = $this->serializer->deserialize($request->getContent(), View::class, 'json');
        $errors = $this->validator->validate($view, null, ['post']);
        if (count($errors) > 0) {
            $return = [];
            foreach ($errors as $error) {
                $return[$error->getPropertyPath()] = $error->getMessage();
            }

            return new JsonResponse(['error' => $return], 422);
        }

        $options = 'TRAITEMENTS=GLOBAL_RIGHTS&OBJET_TYPE=dataset&uuid='.$view->getResource();
        $right = $this->adminClientService->curlAdminWithAdmincli(
            $this->parameterBag->get('ADMIN_URL_API').'/internal/verify_right?'.$options,
            'GET'
        );

        if (is_array($right) && array_key_exists('error', $right)) {
            return new JsonResponse([
                'error' => "ERREUR : Vos droits n'ont pas pu être récupérés",
                'success' => false,
            ]);
        }

        if ($view->isCustomized() && (!isset($right['right']['ADMINISTRATION']) || $right['right']['ADMINISTRATION'] !== true)) {
            return new JsonResponse([
                'error' => "ERREUR : Vous n'avez pas les droits de créer une vue personnalisée",
                'success' => false,
            ]);
        }

        if (isset($right['right']['CMS']) && $right['right']['CMS'] === true) {
            try {
                $viewId = $this->catalogueService->getViewFromUuid($view->getResource());
                $c_view = $this->factory->createCompositeView(
                    intval($viewId),
                    $view->getViewName(),
                    $view->getDataset(),
                    false,
                    true,
                    true,
                    $view->isCustomized()
                );
                $c_view->storeInDb();
                $this->prodige->isTransactionActive() && $this->prodige->commit();
            } catch (\Exception $exception) {
                $this->prodige->isTransactionActive() && $this->prodige->rollBack();

                return new JsonResponse(['error' => $exception->getMessage()], 400);
            }

            return new JsonResponse(['success' => 'View successfully created']);
        } else {
            if (!array_key_exists('fmeta_id', $right['right'])) {
                $response = [
                    'error' => "ERREUR : la donnée ".$view->getResource()." n'existe pas",
                    'success' => false,
                ];
            } else {
                $response = [
                    'error' => "ERREUR : Vos droits ne vous permettent pas d'accéder à la donnée ".$view->getResource(),
                    'status' => 403,
                ];
            }
        }

        return new JsonResponse($response);
    }

    /**
     * @throws Exception
     */
    #[Route('/{uuid}', name: 'patch_view', methods: ['PUT'])]
    #[OA\RequestBody(required: true, content: new OA\JsonContent(ref: new Model(type: View::class, groups: ['patch'])))]
    #[OA\Tag('View')]
    public function patchViewAction(string $uuid, Request $request): Response
    {
        $options = 'TRAITEMENTS=GLOBAL_RIGHTS&OBJET_TYPE=dataset&uuid='.$uuid;
        $right = $this->adminClientService->curlAdminWithAdmincli(
            $this->parameterBag->get('ADMIN_URL_API').'/internal/verify_right?'.$options,
            'GET'
        );

        if (is_array($right) && array_key_exists('error', $right)) {
            return new JsonResponse([
                'error' => "ERREUR : Vos droits n'ont pas pu être récupérés",
                'success' => false,
            ]);
        }

        if (isset($right['right']['CMS']) && $right['right']['CMS'] === true) {
            $view = $this->serializer->deserialize(
                $request->getContent(),
                View::class,
                'json',
                [AbstractObjectNormalizer::PRESERVE_EMPTY_OBJECTS => true]
            );
            $errors = $this->validator->validate($view, null, ['patch']);
            if (count($errors) > 0) {
                $return = [];
                foreach ($errors as $error) {
                    $return[$error->getPropertyPath()] = $error->getMessage();
                }

                return new JsonResponse(['error' => $return], 422);
            }

            $viewId = $this->catalogueService->getViewFromUuid($uuid);
            $c_view = $this->factory->loadCompositeView($viewId);
            $viewTable = $c_view->getCurrentView();

            if ($viewTable->isCustomizedView()) {
                if (isset($right['right']['ADMINISTRATION']) && $right['right']['ADMINISTRATION'] === true) {
                    // Update viewSql
                    $sqlView = $view->getViewSql();
                    [$valid, $error] = $this->catalogueService->validateCustomizedSQLView($sqlView, $view->getDataset());
                    if (!$valid) {
                        return new JsonResponse(['error' => "SQL non valide : $error"], 422);
                    }
                } else {
                    return new JsonResponse(['error' => "Vous n'avez pas les droits"], 403);
                }

                $viewTable->updateSQL($sqlView);
            } else {
                // Remove grouping
                $c_view->removeAllGroupings();

                // Remove computed fields
                $viewTable->removeAllComputedFields();

                // Remove joins
                $viewTable->removeAllJoins();

                // Add joins
                foreach ($view->getJoins() as $key => $join) {
                    $join = $this->serializer->deserialize(json_encode($join), Join::class, 'json');
                    $errors = $this->validator->validate($join, null, ['patch']);
                    if (count($errors) > 0) {
                        $return = [];
                        foreach ($errors as $error) {
                            $return[$error->getPropertyPath()] = $error->getMessage();
                        }

                        return new JsonResponse(['error' => $return], 422);
                    }

                    $tableId = $this->catalogueService->getTableFromUuid($join->resource);
                    $joinTable = $this->catalogueService->getTableMetadataFromId($tableId);
                    $joinCriteria = ViewFactory::buildJoinCriteria($join->fieldView, $join->fieldTable, $key);
                    $joinRestrictive = $join->isRestrictive(
                    ) ? ViewFactory::$T_JOIN_RESTRICTED : ViewFactory::$T_JOIN_UNRESTRICTED;
                    $viewTable->createJoin(
                        $joinTable['storage_path'],
                        $joinRestrictive,
                        $joinCriteria,
                        $join->getVisibleFields()
                    );
                }

                // Add computed fields
                foreach ($view->getCalculatedFields() as $key => $calculatedField) {
                    $viewTable->createComputedField($calculatedField['name'], $calculatedField['expression']);
                }

                // Sort
                if (count($view->getSort())) {
                    foreach ($view->getSort() as $sort) {
                        $viewTable->updateSorting($sort['field'], $sort['method']);
                    }
                } else {
                    $viewTable->updateSorting('null', 'null');
                }

                // Grouping
                $c_view->createGrouping($view->getGroupingMethod(), $view->getGroupingFields());

                // Visible fields
                $viewTable->setVisibleFields($view->getVisibleFields());

                // Filter
                $errMsg = "";
                $checked = $viewTable->checkFilterExpression($view->getFilter(), $errMsg);

                if (!$checked) {
                    $msg = "Il y a une erreur dans l'expression \n".$errMsg;

                    return new JsonResponse(['error' => $msg], 400);
                }

                $viewTable->setFilterExpression($view->getFilter());
            }

            $viewTable->removeFromDb();
            $viewTable->storeInDb();

            $c_view->removeFromDb();
            $c_view->storeInDb();

            return new JsonResponse(['success' => 'View successfully updated']);
        } else {
            if (!array_key_exists('fmeta_id', $right['right'])) {
                $response = [
                    'error' => "ERREUR : la donnée ".$uuid." n'existe pas",
                    'success' => false,
                ];
            } else {
                $response = [
                    'error' => "ERREUR : Vos droits ne vous permettent pas d'accéder à la donnée ".$uuid,
                    'status' => 403,
                ];
            }
        }

        return new JsonResponse($response);
    }

    /**
     * @throws Exception
     */
    #[Route('/{uuid}/recreate', name: 'recreate_view', methods: ['GET'])]
    #[OA\Tag('View')]
    public function recreateViewAction(string $uuid, Request $request): Response
    {
        $options = 'TRAITEMENTS=CMS&OBJET_TYPE=dataset&uuid='.$uuid;
        $right = $this->adminClientService->curlAdminWithAdmincli(
            $this->parameterBag->get('ADMIN_URL_API').'/internal/verify_right?'.$options,
            'GET'
        );

        if (is_array($right) && array_key_exists('error', $right)) {
            return new JsonResponse([
                'error' => "ERREUR : Vos droits n'ont pas pu être récupérés",
                'success' => false,
            ]);
        }

        if (isset($right['right']['CMS']) && $right['right']['CMS'] === true) {

            $viewId = $this->catalogueService->getViewFromUuid($uuid);
            $c_view = $this->factory->loadCompositeView($viewId);

            $viewTable = $c_view->getCurrentView();

            $viewTable->removeFromDb();
            $viewTable->storeInDb();

            $c_view->removeFromDb();
            $c_view->storeInDb();

            return new JsonResponse(['success' => 'View successfully re-created']);
        } else {
            if (!array_key_exists('fmeta_id', $right['right'])) {
                $response = [
                    'error' => "ERREUR : la donnée ".$uuid." n'existe pas",
                    'success' => false,
                ];
            } else {
                $response = [
                    'error' => "ERREUR : Vos droits ne vous permettent pas d'accéder à la donnée ".$uuid,
                    'status' => 403,
                ];
            }
        }

        return new JsonResponse($response);
    }


    /**
     * @throws Exception
     */
    #[Route('/{uuid}', name: 'delete_view', methods: ['DELETE'])]
    #[OA\Tag('View')]
    public function deleteViewAction(string $uuid): Response
    {
        $options = 'TRAITEMENTS=CMS&OBJET_TYPE=dataset&uuid='.$uuid;
        $right = $this->adminClientService->curlAdminWithAdmincli(
            $this->parameterBag->get('ADMIN_URL_API').'/internal/verify_right?'.$options,
            'GET'
        );

        if (is_array($right) && array_key_exists('error', $right)) {
            return new JsonResponse([
                'error' => "ERREUR : Vos droits n'ont pas pu être récupérés",
                'success' => false,
            ]);
        }

        if (isset($right['right']['CMS']) && $right['right']['CMS'] === true) {
            // Get view
            $viewId = $this->catalogueService->getViewFromUuid($uuid);
            $c_view = $this->factory->loadCompositeView($viewId);

            // Remove View
            $this->factory->removeCompositeView($viewId);
            $this->factory->removeView($viewId);

            return new JsonResponse(['success' => 'View successfully deleted']);
        } else {
            if (!array_key_exists('fmeta_id', $right['right'])) {
                $response = [
                    'error' => "ERREUR : la donnée ".$uuid." n'existe pas",
                    'success' => false,
                ];
            } else {
                $response = [
                    'error' => "ERREUR : Vos droits ne vous permettent pas d'accéder à la donnée ".$uuid,
                    'status' => 403,
                ];
            }
        }

        return new JsonResponse($response);
    }

    /**
     * @throws Exception
     */
    #[Route('/data/{uuid}/{limit}', name: 'get_view_data', requirements: ['limit' => '\d+'], methods: ['GET'])]
    #[OA\Tag('View')]
    public function getViewDataAction(string $uuid, int $limit = 20): Response
    {
        $options = 'TRAITEMENTS=CMS&OBJET_TYPE=dataset&uuid='.$uuid;
        $right = $this->adminClientService->curlAdminWithAdmincli(
            $this->parameterBag->get('ADMIN_URL_API').'/internal/verify_right?'.$options,
            'GET'
        );

        if (is_array($right) && array_key_exists('error', $right)) {
            return new JsonResponse([
                'error' => "ERREUR : Vos droits n'ont pas eu être récupérés",
                'success' => false,
            ]);
        }

        if (isset($right['right']['CMS']) && $right['right']['CMS'] === true) {
            $viewId = $this->catalogueService->getViewFromUuid($uuid);
            $view = $this->factory->loadView($viewId);
            $res = $view->getDataAsGeoJson($limit, false);

            return new JsonResponse($res);
        } else {
            if (!array_key_exists('fmeta_id', $right['right'])) {
                $response = [
                    'error' => "ERREUR : la donnée ".$uuid." n'existe pas",
                    'success' => false,
                ];
            } else {
                $response = [
                    'error' => "ERREUR : Vos droits ne vous permettent pas d'accéder à la donnée ".$uuid,
                    'status' => 403,
                ];
            }
        }

        return new JsonResponse($response);
    }
}
