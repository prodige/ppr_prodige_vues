<?php

namespace App\Service;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use Doctrine\Persistence\ManagerRegistry;

class CatalogueService
{
    private Connection $prodige;

    private Connection $catalogue;

    /**
     * @throws Exception
     */
    public function __construct(ManagerRegistry $doctrine)
    {
        $this->prodige = $doctrine->getConnection('prodige');
        $this->catalogue = $doctrine->getConnection('catalogue');
        $this->prodige->isTransactionActive() && $this->prodige->beginTransaction();
    }

    /**
     * Get View primary key from catalogue public metadata uuid
     *
     * @param string $uuid
     * @return int|bool
     * @throws Exception
     */
    public function getViewFromUuid(string $uuid): int|bool
    {
        $sql = "SELECT id FROM public.metadata WHERE uuid = :uuid";
        $stmt = $this->catalogue->prepare($sql);
        $stmt->bindValue('uuid', $uuid);
        $viewId = $stmt->executeQuery()->fetchAssociative();

        return ($viewId && array_key_exists('id', $viewId)) ? $viewId['id'] : false;
    }

    /**
     * Get View uuid from catalogue public metadata primary key
     *
     * @param int $pkView
     * @return string|bool
     * @throws Exception
     */
    public function getUuidFromView(int $pkView): string|bool
    {
        $sql = "SELECT uuid FROM public.metadata WHERE id = :pk_view";
        $stmt = $this->catalogue->prepare($sql);
        $stmt->bindValue('pk_view', $pkView);
        $viewId = $stmt->executeQuery()->fetchAssociative();

        return array_key_exists('uuid', $viewId) ? $viewId['uuid'] : false;
    }

    /**
     * Get Layer table from catalogue public metadata uuid
     *
     * @param string $uuid
     * @return int|bool
     * @throws Exception
     */
    public function getTableFromUuid(string $uuid): int|bool
    {
        $sql = "SELECT id FROM public.metadata WHERE uuid = :uuid";
        $stmt = $this->catalogue->prepare($sql);
        $stmt->bindValue('uuid', $uuid);
        $viewId = $stmt->executeQuery()->fetchAssociative();

        return (array_key_exists('id', $viewId)) ? $viewId['id'] : false;
    }

    /**
     * Get Layer table from catalogue public metadata uuid
     *
     * @param string $id
     * @return array
     * @throws Exception
     */
    public function getTableMetadataFromId(string $id): array
    {
        $sql = "SELECT l.* FROM admin.layer l
                JOIN admin.metadata_sheet ms ON ms.id = l.metadata_sheet_id
                WHERE ms.public_metadata_id = :id";
        $stmt = $this->catalogue->prepare($sql);
        $stmt->bindValue('id', $id);

        return $stmt->executeQuery()->fetchAssociative();
    }

    /**
     * Get Layer table from catalogue public metadata uuid
     *
     * @param string $table
     * @return string|bool
     * @throws Exception
     */
    public function getUuidFromTable(string $table): string|bool
    {
        $sql = "SELECT public_metadata_id FROM admin.layer l
                JOIN admin.metadata_sheet ms ON ms.id = l.metadata_sheet_id
                WHERE storage_path = :name";
        $stmt = $this->catalogue->prepare($sql);
        $stmt->bindValue('name', $table);
        $metadataId = $stmt->executeQuery()->fetchAssociative();
        if (array_key_exists('public_metadata_id', $metadataId)) {
            $sql = "SELECT uuid FROM public.metadata WHERE id = :id";
            $stmt = $this->catalogue->prepare($sql);
            $stmt->bindValue('id', $metadataId['public_metadata_id']);
            $uuid = $stmt->executeQuery()->fetchAssociative();

            return (array_key_exists('uuid', $uuid)) ? $uuid['uuid'] : false;
        }

        return false;
    }

    /**
     * Get View information
     *
     * @param int $viewId
     * @return array|false
     * @throws Exception
     */
    public function getViewInfo(int $viewId): array|false
    {
        $sql = "SELECT vci.view_name, vi.layer_name, vi.sorting_field, vi.view_type, vi.sorting_method, vi.filter_expression, vi.layer_fields, vi.view_sql
                FROM parametrage.prodige_view_info vi 
                JOIN parametrage.prodige_view_composite_info vci ON vci.pk_view = vi.pk_view
                WHERE vi.pk_view = :pk_view";
        $stmt = $this->prodige->prepare($sql);
        $stmt->bindValue('pk_view', $viewId);

        return $stmt->executeQuery()->fetchAssociative();
    }

    /**
     * Get View joins infos (order by join_pos)
     *
     * @param int $viewId
     * @return array
     * @throws Exception
     */
    public function getViewJoins(int $viewId): array
    {
        $sql = "SELECT * FROM parametrage.prodige_join_info WHERE pk_view = :pk_view ORDER BY join_pos ASC";
        $stmt = $this->prodige->prepare($sql);
        $stmt->bindValue('pk_view', $viewId);
        $joins = $stmt->executeQuery()->fetchAllAssociative();
        $viewJoins = [];
        foreach ($joins as $join) {
            if (preg_match('#\[(.*)?\] = \[(.*)?\]#', $join['join_criteria'], $matches)) {
                if ($uuid = $this->getUuidFromTable($join['table_name'])) {
                    $viewJoins[] = [
                        'resource' => $uuid,
                        'dataset' => $join['table_name'],
                        'field_view' => $matches[1],
                        'field_table' => $matches[2],
                        'restrictive' => ($join['join_type'] == '0'),
                        'visible_fields' => explode(',', $join['table_fields']),
                    ];
                }
            }
        }

        return $viewJoins;
    }

    /**
     * Get View calculated fields
     *
     * @param int $viewId
     * @return array
     * @throws Exception
     */
    public function getViewCalculatedFields(int $viewId): array
    {
        $sql = "SELECT * FROM parametrage.prodige_computed_field_info WHERE pk_view = :pk_view";
        $stmt = $this->prodige->prepare($sql);
        $stmt->bindValue('pk_view', $viewId);
        $calculatedFields = $stmt->executeQuery()->fetchAllAssociative();
        $viewCalculatedFields = [];
        foreach ($calculatedFields as $calculatedField) {
            $viewCalculatedFields[] = [
                'name' => $calculatedField['field_name'],
                'expression' => $calculatedField['expression'],
                'type' => $calculatedField['fieldtype'],
            ];
        }

        return $viewCalculatedFields;
    }

    /**
     * Get View aggregated fields
     *
     * @param int $viewId
     * @return array
     * @throws Exception
     */
    public function getAggregateFields(int $viewId): array
    {
        $sql = "SELECT * FROM parametrage.prodige_grouping_aggregate_info WHERE pk_view = :pk_view";
        $stmt = $this->prodige->prepare($sql);
        $stmt->bindValue('pk_view', $viewId);
        $aggregatedFields = $stmt->executeQuery()->fetchAllAssociative();
        $groupingFields = $groupingMethod = [];
        foreach ($aggregatedFields as $aggregatedField) {
            if (is_null($aggregatedField['group_method'])) {
                $groupingFields[] = $aggregatedField['field_name'];
            } else {
                $groupingMethod[$aggregatedField['field_name']] = $aggregatedField['group_method'];
            }
        }

        return ['grouping_fields' => $groupingFields, 'grouping_method' => $groupingMethod];
    }

    /**
     * Validate SQL View for customized view
     *
     * @param string $sql
     * @return array
     */
    public function validateCustomizedSQLView(string $sql, string $dataset): array
    {
        $forbiddenTerms = ['UPDATE', 'DELETE', 'INSERT', 'DROP', 'CREATE', ';'];
        if (preg_match('/'.implode('|', $forbiddenTerms).'/i', $sql)) {
            return [false, 'Forbidden term detected.'];
        }

        $forbiddenSchema = ['carmen.', 'bdterr.', 'local_data.', 'parametrage.', 'topology.' ];
        if (preg_match('/'.implode('|', $forbiddenSchema).'/i', $sql)) {
            return [false, 'Forbidden schema detected.'];
        }


        return [true, null];
    }

}
