/**
 * Retrieve url param from name (empty string if not set)
 * @param sParam
 * @returns string
 */
function getUrlParameter(sParam) {
    let sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? '' : decodeURIComponent(sParameterName[1]);
        }
    }
    return '';
}

/**
 * Get suffix language for localization (datatable)
 * @returns string
 */
function getLanguage() {
    let supportedLanguages = ['en', 'fr'],
        language = (navigator.languages !== undefined) ? navigator.languages[0] : navigator.language;
    return ($.inArray(language, supportedLanguages) !== -1) ? language : 'en';
}

/**
 * Trigger dynamic activation
 */
triggerActive = function () {
    let attr = $(this).attr('data-trigger-active');
    if ($(this)[0].nodeName.toLowerCase() === 'select') {
        let selectedValue = $(this).val();
        $("[" + attr + "]").each(function () {
            if (selectedValue === $(this).attr('data-trigger-value')) {
                $(this).removeAttr('disabled').attr('required', 'required');
            } else {
                $(this).attr('disabled', 'disabled').removeAttr('required').val('');
            }
        });
    } else {
        let checked = $(this).is(':checked');
        $("[" + attr + "]").each(function () {
            if (checked) {
                $(this).removeAttr('disabled');
            } else {
                $(this).attr('disabled', 'disabled');
            }
        });
    }
}

/**
 * Trigger dynamic display
 */
triggerDisplay = function () {
    let attr = $(this).attr('data-trigger-display');
    let checked = $(this).is(':checked');
    $("[" + attr + "]").each(function () {
        if (checked) {
            $(this).parent().show();
        } else {
            $(this).val('').parent().hide();
        }
    });
}


/**
 * Trigger select dynamic display
 */
triggerSelectDisplay = function () {
    let attr = $(this).attr('data-trigger-select-display');
    let value = $(this).val();
    $("[" + attr + "]").each(function () {
        if ($(this).attr(attr) === value) {
            $(this).parent().show();
        } else {
            $(this).parent().hide();
        }
    });
}

/**
 * Trigger duallist required select
 */
initDualListBox = function (duallistbox) {
    var instance = duallistbox.data('plugin_bootstrapDualListbox');
    var nonSelectedList = instance.elements.select1;
    var isDualListBoxValidated = !(instance.selectedElements > 0);
    nonSelectedList.prop('required', isDualListBoxValidated);
    instance.elements.originalSelect.prop('required', false);
}