#!/bin/bash
set -e

# Source env file
SCRIPT=`readlink -f $0`
SCRIPTDIR=`dirname $SCRIPT`
SCRIPTNAME=`basename $SCRIPT`

cd ${SCRIPTDIR}/../site

# Wait postgresql restart
sleep 3

#
OWNER="www-data"
function run_as {
  su -pc "$1" -s /bin/bash $OWNER
}
