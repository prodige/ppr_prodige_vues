# Module optionnel PRODIGE Vues

Module intégrant un back-office de configuration et une API à destination d'une interface de paramétrage des jointures de données.

### Installation
[documentation d'installation en production](cicd/prod/README.md).

[documentation d'installation en développement](cicd/dev/README.md).

## Accès à l'application
- [Accès au back-office](https://vues.prodige.internal/).
- [Accès à l'API](https://vues.prodige.internal/api/doc/).
